package com.hnvec.fuxiao;

/*


字符串的常见操作
 */
public class Example01 {

    public static void main(String[] args) {

        String str= new String("abcacdqweasddfbqaqa");
        System.out.println("eas第一次出现的索引"+str.indexOf("eas"));
        System.out.println("长度"+str.length());
        System.out.println("qa最后一次出现的索引"+str.lastIndexOf("qa"));
        System.out.println("返回下标为7的字符"+str.charAt(7));
        System.out.println("是否以aqa结尾"+str.endsWith("aqa"));
        System.out.println("打印输出abcacd是否与字符串相等"+str.equals("abcacd"));
        System.out.println("判断当前字符串是否为空"+str.isEmpty());
        System.out.println("是否以adc开头"+str.startsWith("abc"));
        System.out.println("判断是否包含we"+str.contains("we"));
        System.out.println("转换为大写"+str.toUpperCase());
        System.out.println("int参数表示形式"+str.valueOf(3));/*取别名*/




    }



}
