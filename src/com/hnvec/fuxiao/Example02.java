package com.hnvec.fuxiao;
/*
字符串截取和替换。去首尾空格

 */
public class Example02 {
    public static void main(String[] args) {
            String str ="13785429465";
            System.out.println("将手机号中间四位隐藏用*代替"+str.replace("8542","****"));
            String str1 ="010-152-1988-1012";
            String[] strings=str1.split("-");
            for(int i=0; i < strings.length;i++){
                System.out.println(strings[i]);
            }

        System.out.println("截取手机后四位"+str.substring(7));
        System.out.println("截取手机号中间四位"+str.substring(3,7));
        String str2="        123456789        ";
        System.out.println("去除两端空格"+str2.trim());
    }
}
