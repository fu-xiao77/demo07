package com.hnvec.fuxiao;
/*


对于StringBuffer[字符缓冲区] 进行增删改的操作
String和StringBuffer的区别为后者内容和长度可以改变

 */

public class Example03 {

    public static void main(String[] args) {

        System.out.println("添加-----------");
        add();
        System.out.println("删除-----------");
        remove();
        System.out.println("修改-----------");
        alter();
    }

    public static void add(){
        StringBuffer stringBuffer =new StringBuffer();//创建一个字符串缓冲区
        stringBuffer.append("周仁龙");//在原字符串末尾处添加新的字符串内容
        stringBuffer.append("20级大数据一班");
        System.out.println(stringBuffer);//周仁龙20级大数据一班
        stringBuffer.insert(0,"湖南网络工程职业学院");
        System.out.println(stringBuffer);
    }
    public static void remove(){
        StringBuffer stringBuffer=new StringBuffer("湖南网络工程职业学院");
        stringBuffer.delete(0,2);
        stringBuffer.delete(2,7);
        System.out.println(stringBuffer);
        stringBuffer.deleteCharAt(1);
        System.out.println(stringBuffer);

    }
    public static void alter(){
        StringBuffer stringBuffer =new StringBuffer("湖南开放大学大数据应用与技术一班");
        stringBuffer.replace(2,6,"网络工程职业学院");
        System.out.println(stringBuffer);//湖南网络工程职业学院大数据应用与技术一班
        stringBuffer.setCharAt(18,'二');
        System.out.println(stringBuffer);
    }
}


